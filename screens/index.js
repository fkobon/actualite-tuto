import React, { Component } from "react";
import Home from "./Home";
import SideBar from "./SideBar.js";
import Page2 from "./Page2";
import { createStackNavigator,createDrawerNavigator } from "react-navigation";

const HomeScreenRouter = createDrawerNavigator( 
  {
    Home: { screen: Home },
 },
  {
  	navigationOptions: {
    drawerLockMode: 'locked-closed',
 	 },
    contentComponent: props => <SideBar {...props} />
   
  }

);

const App =  createStackNavigator(
  {
    Home:HomeScreenRouter,
    Page2:Page2,
  },
  {
    headerMode: 'none',
   
  }
);

export default App;

